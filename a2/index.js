//Modify the Student class to have a new constructor property named "passed" and another new constructor property named "passedWithHonors." Both properties should be given a value of undefined by default.

//Modify our willPass() and willPassWithHonors() methods from the previous session to be both chainable, and assign the values they return to the new "passed" and "passedWithHonors" properties in our constructor

class Student {
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;
    this.gradeAve = undefined;
    this.passed = undefined;
    this.passedWithHonors = undefined;

    if (grades.length === 4) {
      if (grades.every((grade) => typeof grade === 'number')) {
        if (grades.every((grade) => grade >= 0 && grade <= 100)) {
          this.grades = grades;
        } else {
          this.grades = undefined;
        }
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }
  }
  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }
  listGrades() {
    console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
    return this;
  }
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    this.gradeAve = sum / 4;
    return this;
  }

  willPass() {
    if (this.computeAve() >= 85) {
      this.passed = true;
      return this;
    } else {
      this.passed = false;
      return this;
    }
  }
  willPassWithHonors() {
    this.computeAve();
    if (this.gradeAve >= 90) {
      this.passedWithHonors = true;
      return this;
    } else if (this.gradeAve >= 85) {
      this.passedWithHonors = false;
      return this;
    } else {
      this.passedWithHonors = undefined;
      return this;
    }
  }
}

let studentOne = new Student('John', 'john@email.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@email.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@email.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@email.com', [91, 89, 82, 93]);

// console.log(studentOne);
// console.log(studentTwo);
// console.log(studentThree);
// console.log(studentFour);

// console.log(studentOne.willPass());
// console.log(studentOne.willPassWithHonors());
// console.log(studentTwo.willPass());
// console.log(studentTwo.willPassWithHonors());
// console.log(studentThree.willPass());
// console.log(studentThree.willPassWithHonors());
// console.log(studentFour.willPass());
// console.log(studentFour.willPassWithHonors());
